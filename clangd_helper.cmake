# private macro to extract include folders from target
# - get the include directories for the target.
# - filter out the generator statements
# - construct the compiler flags for the include directories.
macro(_CreateClangdIncludes SRCTARGET DESTINCLUDES)
    get_target_property(LIB_INCLUDES ${SRCTARGET} INCLUDE_DIRECTORIES)
    #if it's imported target scan for that too
    get_target_property(LIB_IMPORTED_INCLUDES ${SRCTARGET} INTERFACE_INCLUDE_DIRECTORIES)
    foreach(_dir ${LIB_INCLUDES} ${LIB_IMPORTED_INCLUDES})
        if(NOT ${_dir} MATCHES ".*NOTFOUND")
            string(REGEX REPLACE "\\$<.*:(.*)>" "\\1" dir ${_dir})
            list(APPEND ${DESTINCLUDES} "-I${dir}")
        endif()
    endforeach()
endmacro()



# Call this function to create a new (it's a dumb function)
# .clangd in the source folder.
# don't forget to give clangd the --enable-config flag
# call this macro with all targets you wish to inspect as parameters.
macro(WriteDotClangd)
    unset(MYINCLUDES)
    unset(outflags)
    foreach(LIB ${ARGV})
        _CreateClangdIncludes(${LIB} MYINCLUDES)
    endforeach()
    list(REMOVE_DUPLICATES MYINCLUDES)
    list(JOIN MYINCLUDES ", " includes)

    file(WRITE "${CMAKE_SOURCE_DIR}/.clangd"
"CompileFlags:
    Add: [ ${includes} ]
")
endmacro()
