Introduction
-------------------
Just a few macros and/or functions to assist in software development. 
I licenced them as MIT so feel free to use them as you see fit. Also feel free to help improve them.

clangd_helper.cmake
------------------
Use this in conjunction with cmake's CMAKE_EXPORT_COMPILE_COMMANDS to wrangle
clangd into submission without the aid of even more tools.

Call the macro WriteDotClangd() with all targets you wish to inspect as parameters. It will output a `.clangd` file in the `${CMAKE_SOURCE_DIR}` overwriting any previous file.
